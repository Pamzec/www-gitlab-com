---
layout: handbook-page-toc
title: "Brand Activation Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand Activation Handbook
{:.no_toc}

## Teams
- [Corporate events](https://about.gitlab.com/handbook/marketing/corporate-marketing/#corporate-events) 
- Brand design - [Handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/)
- Brand growth - [Handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-growth/) 

## Brand Guidelines 
See our [brand guidelines handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-standards/) 

For any questions you can reach us on Slack at #brand-activation
