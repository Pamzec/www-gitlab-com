---
layout: handbook-page-toc
title: "GitLab Patent Program"
description: "Learn about GitLab's Patent Program"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Innovation at GitLab

Innovation is key to GitLab’s success. The GitLab Patent Program seeks to maximize the competitive and commercial value of GitLab’s intellectual property rights and capitalize on the time and effort invested by team members in developing patentable inventions. The Program establishes a process to systematically identify valuable inventions, assess their patentability and, where appropriate, enable successful prosecution and registration of a patent covering the invention.

## Why patents?

Seeking patent protection for inventions offers several benefits. For individual team members, appearing as a named inventor on a published patent is a significant achievement and something that provides tangible evidence of team members’ creative contributions to GitLab and emphasizes GitLab’s [focus on results](https://about.gitlab.com/handbook/values/#results).

For GitLab, building a portfolio of patents helps increase our profile as innovators in the DevOps space. Moreover, patents are valuable assets, and an investment in building a patent portfolio can increase value for shareholders. In some regions, obtaining patent protection for inventions renders the related research and development costs tax deductible.

Patents can also be used as a defensive tool in the event the owner of another patent seeks to assert their patent rights against GitLab.

## GitLab patents
GitLab holds the following registered patents:
- US patent [9,910,766](https://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=1&f=G&l=50&co1=AND&d=PTXT&s1=%22peach+fuzzer%22.ASNM.&OS=AN/%22peach+fuzzer%22&RS=AN/%22peach+fuzzer%22) covering _whitebox network fuzzing_
- US patents [9,767,005](https://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=2&f=G&l=50&co1=AND&d=PTXT&s1=%22peach+fuzzer%22.ASNM.&OS=AN/%22peach+fuzzer%22&RS=AN/%22peach+fuzzer%22) and [9,098,352](https://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=1&f=G&l=50&co1=AND&d=PTXT&s1=9098352.PN.&OS=PN/9098352&RS=PN/9098352), Chinese patent [104346277A](http://ensearch.cnipr.com.cn/sipo_EN/search/detail.do?method=view&parm=16b414c21a2f19d11b2c18401bcd1a5f183a19461ad91be11a601c4c07792e231f4c21ba22572195236c20482755275723ca24be2221222525702494250d26c0274025822c3f29092a7c29a02d6d2d6f28fa2ed62bd12c892f482c34330947932f5c2c0a2ac731b9333c316c366534e7318235ee337934f1360837f03747371b371a34e231bf38f13b04390c3e0d3f6f39ea3dc63c573d6d3e683e4c26d918c33ffe3c523c273e354334405c47c545774302408e4423458947b84688462d46a946c846aa436f47554bcc49784a2d), and pending European patent application [14175419.2](https://register.epo.org/espacenet/regviewer?AP=14175419&CY=EP&LG=en&DB=REG) covering _metaphor based language fuzzing of computer code_
- US patent [9,323,597](https://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=3&f=G&l=50&co1=AND&d=PTXT&s1=%22peach+fuzzer%22.ASNM.&OS=AN/%22peach+fuzzer%22&RS=AN/%22peach+fuzzer%22) covering _flow based fault testing_

## Patent Awards

As a recognition of the efforts of team members who develop inventions for GitLab, named inventors who contribute patentable inventions under the Patent Program may be eligible for Patent Awards totalling $1,750 U.S. dollars, as follows:
- Level One Patent Award: $250 for each named inventor on each patent application submission accepted for filing by GitLab, paid at the time of acceptance.
- Level Two Patent Award: $500 for each named inventor on each patent application filed by GitLab, paid at the time of filing.
- Level Three Patent Award: $1,000 for each named inventor on each patent issued to GitLab, paid at the time of patent issue.


## How to participate

To disclose an invention, complete an [Invention Disclosure Form](https://docs.google.com/document/d/1BndxggJyA3aMdYav-ghNwpTdNcfgJcrHFRlT6u8KFD8/copy) and email it to [patent-program@gitlab.com](mailto:patent-program@gitlab.com).

You should submit an Invention Disclosure Form any time you think you may have made an invention. **The sooner you submit the Form, the better.** Since you may be overly conservative in determining what constitutes an invention, you should submit a Form even if you aren’t sure that your work is sufficiently novel to be an invention. So, err on the side of disclosing, leaving it to the Legal Team, and our external patent specialists, to decide whether to file, retain as a trade secret, or publish to preempt.

In order to maintain the confidentiality of your invention, do not discuss it on a public or even confidential GitLab issue or on any slack channels - all correspondence concerning the invention should take place via [patent-program@gitlab.com](mailto:patent-program@gitlab.com).


## Conditions of participation

Inventors must be current GitLab Team Members in order to participate in the GitLab Patent Program. GitLab may change or terminate the Program at any time. GitLab reserves the right in its sole discretion to decide whether to file for a patent on submissions or retain submissions as a trade secret or publish to preempt. Participation in the GitLab Patent Program is subject to local employment and intellectual property laws.
