---
layout: handbook-page-toc
title: "Mental Health Awarness at GitLab"
canonical_path: "/company/culture/all-remote/mental-health/mental-health-awareness-learning-path/"
description: Learn what resources are available for GitLab team members to care for their mental health
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

This learning path has been adapted from content shared in the 2020 Mental Health Awareness week hosted by the GitLab Learning and Development team. During this week, the L&D team curated resources and hosted a live speaker series with the goal of supporting team members in managing their mental health, especially during times of crisis like the Covid-19 pandemic.

Original issues used to communicate this content can be referenced on the [L&D team challenges issue board](https://gitlab.com/gitlab-com/people-group/learning-development/challenges/-/boards).

### Learning Objectives

After completing this learning path, GitLab team members will

1. Understand where and how to access resources at GitLab to support their mental health
1. Recognize the value that taking time off from work can bring to their role and team
1. Contribute mental health skills and resources to the GitLab handbook


## Part 1: Your Mental Health Matters

In an all-remote environment, it can be difficult to identify signals of burnout and distress in ourselves and others. It is common to turn to work as a distraction to the chaos we feel in our day-to-day lives.

We hope you’ll use this opportunity to take a deep dive (or, if you’re not ready, just a shallow dip) into what mental health awareness looks like here at GitLab and how you can take care of yourself and your team. 

Our goals in this learning path, and [ongoing L&D initiatives about mental health](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/151), are:

1. Normalize the discussion about mental health across the GitLab team.
1. Increase access to and awareness of existing mental health resources for the GitLab team.
1. Understand how our asynchronous culture can contribute to improved mental health for team members, specifically by decreasing the total number of synchronous meetings for team members.
1. Increase documentation about mental health management resources in our handbook by collaborating and sharing strategies and tools that work

Take a moment to listen to [this short conversation with Wendy Barnes](https://youtu.be/JG7mQEW5fss), our CPO at GitLab, where we chat about strategies team members can implement to manage their mental health, especially with added stress during the Covid-19 pandemic.

<iframe width="560" height="315" src="https://www.youtube.com/embed/JG7mQEW5fss" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Practicing Gratitude

To kick off the discussion, we’re focusing on gratitude and the role it plays in our overall mental health. Taking a moment to practice [gratitude](/company/culture/all-remote/mental-health/#take-a-moment-for-gratitude) can be a simple and effective strategy to walk away from situations or conversations that are causing us stress, and refocus our attention on joy and gratefulness. 

What are you grateful for today? Consider opening a merge request on our [mental health awareness handbook page](/company/culture/all-remote/mental-health/#take-a-moment-for-gratitude) and share your thoughts about gratefulness.

Did you know that at GitLab, we have a [daily-gratitude](https://app.slack.com/client/T02592416/C010TG0RFN1) channel where team members share experiences, people, and feelings that make them grateful? Consider starring the channel and leaning on your team members there when you need a moment to shift your emotions or focus. Our wider community members can consider creating a similar channel within their organization.


### Resources

1. Take a moment to review the [GitLab PTO policy](/handbook/paid-time-off/)
1. Review our company guidelines around [Combating burnout, isolation, and anxiety in the remote workplace](/company/culture/all-remote/mental-health/)


## Part 2: Prioritize your mental health

### Benefits of caring for your mental health

Taking time to prioritize your mental health can help to

1. Proactively avoid feelings of burnout
1. Address and contribute to the solution of current symptoms of burnout
1. Build trust between team members
1. Normalize conversations about healthy remote practices and boundaries

#### Break for mindfulness

Start by trying a mindfulness break during today’s work day. Choose one of the free meditations from [Headspace](https://www.headspace.com/covid-19) to get started.

Not ready for an audio meditation? Try one of these mindfulness breaks instead.

1. Close your eyes and take 5 of the deepest breaths you’ve taken all day.
1. Sit in your chair while slowly looking around the room. Try to identify items that match each color in the rainbow, starting with red.
1. Stand up and reach your arms high above your head. Gently lean from left to right to stretch the side of your body. - You can also do this movement sitting.

### Take time off from work

One of the most effective ways to manage your mental health is to utilize the [GitLab PTO Policy](/handbook/paid-time-off/) 


Taking time off is important. Here are just a few highlights of the GitLab PTO policy that are in our handbook to remind team members the importance of taking time away from work:

> Not taking vacation is viewed as a weakness and people shouldn't boast about it. It is viewed as a lack of humility about your role, not fostering collaboration in training others in your job, and not being able to document and explain your work.

> It's important to take PTO while you have something you want to do, rather than waiting until you need the time off.

> Remember that it's normal to take extra time to catch up after [returning from paid time off](/handbook/paid-time-off/#returning-from-pto). Taking time off doesn't mean that you need to work multiple extra hours before or after your vacation. When taking extended time off, expect to have reduced capacity to take on new work the week of your return while you're catching up on the work that happened while you were away.

Support from managers to take time off, and to view time off as a way to efficiently reach results, is essential. Wendy Barnes, CPO at GitLab, [speaks to how managers can support their team members in taking time off](https://youtu.be/AM-vogR6SyQ):

<iframe width="560" height="315" src="https://www.youtube.com/embed/AM-vogR6SyQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Take time to review strategies for [returning from PTO](/handbook/paid-time-off/#returning-from-pto) to prepare for the transition back into your work routine.

### How can I leverage our asynchronous working culture to care for my mental health?

1. [How to decline meetings in favor of async?](/company/culture/all-remote/asynchronous/#how-to-decline-meetings-in-favor-of-async)
1. [Remove Slack from your phone](/company/culture/all-remote/asynchronous/#remove-slack-from-your-phone)
1. [Examples of asynchronous communication at GitLab](/company/culture/all-remote/asynchronous/#examples-of-asynchronous-integration-on-gitlab-teams)
1. [Mental health benefits of async work](/company/culture/all-remote/asynchronous/#mental-health)

### I’d like to learn more from my GitLab team — where can I go to talk about my mental health?

1. [`#mental_health_aware` Slack channel](https://app.slack.com/client/T02592416/C834CM4HW)
1. Bi-Monthly Mental Health Social Calls - ask to be added to the invitation in the [`#mental-health-aware` Slack channel](https://app.slack.com/client/T02592416/C834CM4HW)
1. [`#mindfulness` Slack channel](https://app.slack.com/client/T02592416/C019ZL09E7N)
1. [Mindfulness Calls](/handbook/communication/#mindfulness-call)
1. [Weekly GitLab Social Calls](/handbook/communication/#social-call)
1. [1:1 with your manager](/handbook/leadership/1-1/)
1. [Current mental health bookclub planning](https://gitlab.com/gitlab-com/book-clubs/-/issues/14)

### What other resources can I access as a GitLab team member to support my mental health?

1. [Benefits Documentation](/handbook/total-rewards/benefits/)
1. [Modern Health](/handbook/total-rewards/benefits/modern-health/)
1. [Specific policy for Sick related Leave](/handbook/paid-time-off/#sick-time---taking-and-reporting) 
1. [Unpaid Leave of Absence](/handbook/paid-time-off/#unpaid-leave-of-absence)

### Building trust is important to me to feel comfortable and supported — where can I go at GitLab to build new relationships?

1. [Coffee Chats](/company/culture/all-remote/informal-communication/#coffee-chats)
1. [The donut bot](/company/culture/all-remote/informal-communication/#the-donut-bot) 
1. [Slack channels based on interests](/handbook/communication/chat/)
1. [Walk and Talk meeting structure](/handbook/communication/#walk-and-talk-calls)

### What are some great external resources I can access?

1. [Headspace meditation app](https://www.headspace.com/subscriptions)
1. [Modern Health Community Circles](https://circles.modernhealth.com/)
1. [Clockwise App](https://www.getclockwise.com/)
1. [Modern Health Community Sessions, Dec 2 - 22](https://circles.modernhealth.com/series/holidays)

### Have a resource, either internal or external, that you’d like to share? Contribute to the [combating burnout, isolation, and anxiety in the remote workplace page](/company/culture/all-remote/mental-health/#mental-health-tool-stack) and help build out our mental health tool stack!


## Part 3: Recognizing burnout in yourself and others

In an all remote environment, we’re faced with unique challenges when it comes to

1. Identifying burnout in ourselves
1. Identifying signs of burnout from our team members
1. Balancing the achievement of OKRs with the mental health of our team
1. Connecting and empathizing through text and video communication

### Recognizing burnout in ourselves

Identifying and coming to terms with the feeling of burnout in ourselves can be challenging. This self assessment tool called The Burnout Index can help you identify feelings of burnout in yourself.

1. [Burnout Index](https://burnoutindex.org/)

Our handbook outlines feelings and experiences we might notice in ourselves related to burnout, [GitLab Handbook: How to Recognize Mental Health Struggles](/company/culture/all-remote/mental-health/#how-to-recognize-mental-health-struggles). Take a moment to review these indicators and ask yourself if you have felt or identified with any of these statements in the last month:


### Recognizing burnout in others

[See Me Scotland](https://www.youtube.com/user/seemescotland) created a campaign called [The Power of Okay](https://www.youtube.com/watch?v=CC4QzwlmhxQ&feature=youtu.be) 

The campaign is `...all about getting people talking about mental health and being there for each other. If someone you know is struggling with their mental health, ask them if they are okay, and really listen. It could make all the difference.`

Examples of teammate behavior where asking ‘are you okay’ can help:

1. You notice your team members who usually posts often in the #craftlab (or any social channel) has been silent there for a while.
1. A colleague who typically has their camera on in team meetings has kept their camera off for the last few weeks.
1. During a team call, you notice a team member is unusually quiet about a topic where you know they have a strong opinion or don’t agree with the conclusion

Managers play a key role in identifying and supporting team members through burnout. Watch [this short interview with Wendy Barnes](https://youtu.be/dgwaCsyI2vw), where we chat about how managers can support their team members in managing symptoms of burnout.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dgwaCsyI2vw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Other resources and self assessments

1. [8 Consequences of Anxiety that shouldn’t be confused with rudeness](https://www.instagram.com/p/CG4ztD8jfOJ/?igshid=8mq1gqvo8oin)
1. [Burnout assessment](https://ssir.org/pdf/2005WI_Feature_Maslach_Leiter.pdf)

### GitLab resources

1. [Communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/)
1. [All Remote Handbook: Mental Health](/company/culture/all-remote/mental-health/)
1. [Blog post about preventing burnout](/blog/2018/03/08/preventing-burnout/)


## Part 4: Time Off Live Speaker Series with John Fitch

1. [Rest and time off are productive](/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive)
1. [Recorded speaker series with John Fitch](https://youtu.be/BDvpoouM-us)


## Part 5: Mental Health Reflection

Taking time to manage, care for, and support your mental health is important. We hope that this week has helped bring to light resources and strategies you can use to support yourself and your teammates through times of stress, chaos, and burnout. Thank you for taking time to review these resources, contribute to our [Mental Health Tool Stack](/company/culture/all-remote/mental-health/#mental-health-tool-stack), and for being open to having conversations about mental health here at GitLab.

**We invite you to reflect on this Mental Health awareness week by answering the following questions. Consider sharing with a friend, family member, or a colleague.**


### Mental Health Awareness Week Reflection Questions:

1. Which strategy, resource, or skill stood out to you this week? How might this strategy support you in managing burnout?
1. What questions do you still have about prioritizing your mental health?
1. Any additional resources you'd like to see from the L&D team on this topic?
