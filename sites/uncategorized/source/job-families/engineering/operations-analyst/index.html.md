---
layout: job_family_page
title: "Operations Analysts"
---

# Operations Analyst Roles at GitLab

Operations Analysts at GitLab work on business operations and are focused on improving and enhancing business data management and analysis. They work in departments with directors and managers, and across other areas of the organization to liaison on business operations.

## Job Grade

The Operations Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
The Senior Operations Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

Unless otherwise specified, all Operations Analyst roles at GitLab share the following requirements and responsibilities.

<a id="intermediate-requirements"></a>

#### Requirements

- Consistent track record of using quantitative analysis to impact key business decisions
- Proficiency in the English language, with excellent written and oral communication skills
- Positive and solution-oriented mindset
- Excellent analytical skills
- An inclination towards communication, inclusion, and visibility
- Self-motivated and self-managing, with demonstrated organizational skills
- Ability to present data using detailed reports and charts
- Demonstrable strategic thinking skills
- Confidentiality in handling sensitive financial information
- Share our values, and work in accordance with those values
- Ability to thrive in a fully remote organization
- BS degree in Finance, Accounting or Economics
- Ability to use GitLab

#### Nice-to-haves

- Experience in a peak performance organization
- Experience working with a remote team
- Experience working with a global or otherwise multicultural team

#### Responsibilities
 - Develop resource utilization and forecasting models to meet business and financial goals
 - Develop financial models to provide data-focused guidance on cost decisions
 - Interface with various departments to provide timely, accurante and relevant data
 - Review business processes and policies to help enhance workflows
 - Manage, analyze and report departmental KPIs
 * Ability to use GitLab

## Career Ladder

 For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
