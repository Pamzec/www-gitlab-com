title: U.S. Army Cyber School
cover_image: '/images/blogimages/cyberschool.jpg'
cover_title: |
  How the U.S. Army Cyber School created “Courseware as Code” with GitLab 
cover_description: |
  The U.S. Army Cyber School created secure, collaborative coursework with GitLab CI/CD, DevOps, and SCM.
twitter_image: '/images/blogimages/cyberschool.jpg'

twitter_text: 'Learn how the U.S. Army Cyber School created "Courseware as Code" with GitLab’s all-in-one solution.'

customer_logo: '/images/case_study_logos/usarmy.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Government 
customer_location: Georgia
customer_solution: GitLab Ultimate
customer_employees: Over 200 Personnel 
customer_overview: |
  The U.S. Army Cyber School wanted to build its software development using Git and CI and found success with GitLab’s all-in-one solution. 
customer_challenge: |
  The U.S. Army Cyber School lacked a secure, singular platform for Git and CI that both students and teachers could access. 

key_benefits:
  - |
    Code ownership with SCM 
  - |
    Improved collaboration
  - |
    Enhanced CI integration
  - |
    Easy template creation
  - |
    GitLab Issues, Boards, and Epics drive course progress

customer_stats:
  - stat: 1  
    label: Month review cycle time, down from 3 years
  - stat: 8,415    
    label: Commits by students and contributors 
  - stat: 6     
    label: U.S. Army Cyber School courses created using GitLab


customer_study_content:
  - title: the customer
    subtitle: The U.S. Army Cyber Center of Excellence
    content:
      - |
        The U.S. Army Cyber School is responsible for the functional training and education of all U.S. Army 17 Series Soldiers. The <a href="https://cybercoe.army.mil/CYBERSCH/index.html" target="_blank">U.S. Army Cyber School</a> provides students with the required knowledge, skills, and abilities related to Defensive Cyberspace Operations (DCO), Offensive Cyberspace Operations (OCO), Electronic Warfare (EW), and cyberspace planning, integration, and synchronization.
      
  - title: the challenge
    subtitle: Starting Git from scratch
    content:
      - |
        After being established in 2014, the U.S. Army Cyber School at Fort Gordon was tasked with creating the school’s software development process from the ground up. There wasn’t a legacy system to manage or update, but at the same time, the school had nothing to work from—no instructors, no content, and no playbook to follow.   
      - |
        As the team started to create content for the program, the data was primarily created and stored on individual laptops. That approach quickly became problematic when the school experienced turnover and content was lost when individuals left the organization. Other early tactics included emailing around Zip files and whiteboarding, which resulted in an inefficient and exceedingly manual process. The school also lacked a way for the team to collaborate and track progress on their projects. Without a common repository for the team to contribute to, the school risked losing the valuable content they paid contractors to develop. 
      - | 
        To address their growing challenges, the school drew inspiration from other teams doing software development and began their search for a Git-based solution for their unique situation. The goal was to empower the team with “Courseware as Code” and use DevOps principles like continuous integration and continuous deployment to replace the traditional maintenance of content in documentation styles (presentations, word processing documents, spreadsheets, etc.) with markdown language and CI pipelines.
      - |
        Captain Christopher Apsey and his team wanted one software tool that would allow developers to collaborate on a single corpus of information, a platform for students and staff to use Git and provide a way to [access CI pipelines](/blog/2019/07/12/guide-to-ci-cd-pipelines/). They researched GitHub, Gogs, Node Kitten, Gitea, and ultimately chose GitLab because it offered a variety of services that other platforms didn’t match, including integrated CI. With the collaborative capabilities of GitLab, coursework and certification assessments were established.
    
  - blockquote: Instead of having to teach people 10 different things, they just learn one thing, which makes it much easier in the long run.
    attribution: Chris Apsey  
    attribution_title: Captain, U.S. Army  

  - title: the solution
    subtitle: From a Zip drive to CI pipeline
    content:
      - |
        The U.S. Army Cyber School uses their GitLab instance to administer and grade the programming exam, the first certification assessment for 17 Series Developers in the Army. The CI pipeline is used for grading exams, which is an interactive JavaScript website hosted on GitLab pages that summarizes test results. The process is actively being developed in order to streamline grading and certification through automation and collaboration. 
      - |
        To achieve a successful Courseware as Code deployment, nearly every piece of technical content the school has curated in GitLab has been written in markdown language. This includes student templates, course content, slide decks, and handouts. All of the content production has been CI driven and is completed in the same fashion as software development. Whenever someone makes a change to a document, it is recorded and stored within GitLab as the [single source of truth](/topics/version-control/). 

  - title: the results
    subtitle: SCM, CI/CD, and DevOps for career success
    content:
      - |
        Through the implementation of GitLab’s automated workflow, the U.S. Army Cyber School has established coursework for multi-instructor, multi-contributor, location-disparate classes and has solved many of the limitations that they previously experienced. There have been six courses created using GitLab and over 4,000 merges between instructors and students.  
      - |
        Because everyone contributes within the central repository, no one leaves the school with government information, which had formerly been an exfiltration risk. The school is also benefiting from alumni who can contribute their knowledge from the field and provide real-time field updates. Graduates are encouraged to shape the learning material for current students by submitting merge requests directly into the course repositories.    
      - |
        With DevOps, SCM, and CI/CD firmly in place, all contributors work in a collaborative and transparent environment. Issues, boards, epics, and checklists are heavily used as data tools to increase student participation. These GitLab features allow instructors to easily track how students are progressing. Traditional documentation formats have been replaced with GitLab’s trackable project repositories. “We can now look and say, ‘Who's doing what? Who's value-added? Who's being a team player, who's slacking off?’ all within the GitLab commit history,” CPT Apsey said.  
      - |
        Exams are created using a group-level template with the seed from a student’s examination file. The exam template includes a merge request and issues, so when a new project is created the student has their own repository. “We add them as a developer to the repository and they make commits to any branches they want and merge them into develop or they can make commits straight to develop. It's from that open MR where we have the conversation and grade,” said CPT Jessie Lass, Senior Developer, U.S. Army.
      - |
        Review cycle times previously took three years, but with GitLab’s asynchronous collaboration it now moves much faster. “In GitLab I've been able to tag people and then within a week get feedback. I wouldn't say it's a full formalized review cycle, but certainly from years down to months,” according to CPT Benjamin Allison, Cyberspace Operations Officer.
      - |
        Using GitLab has not only improved the internal development of examinations, but it is also helping students become proficient in Git. “My hope is that these candidates, once we certify them, they'll have had a little bit of exposure to what a healthy professional development workflow looks like. So when they show up onto a team, they're actually ready to work,” CPT Jessie Lass said.
      - |
        <i>Disclaimer: Participation in this survey is in no way to be considered an endorsement of your product by the Department of Defense, nor can you indicate in any product advertisement that such an endorsement exists.</i> 
      
      - |
        ## Learn more about CI/CD, SCM, and DevOps
      - |
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
      - |
        [Optimize your DevOps value stream](/solutions/value-stream-management/)
      - |
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)

